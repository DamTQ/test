<?php

use PHPUnit\Framework\TestCase;

final class Exercice2Test extends TestCase
{
    public function testRun(): void
    {
        $this->assertSame(0, Exercice2::run(1, 1, 1, 1));
        $this->assertSame(0, Exercice2::run(2, 2, 2, 2));
        $this->assertSame(6, Exercice2::run(1, 2, 3, 4));
        $this->assertSame(10, Exercice2::run(2, 2, 3, 11));
        $this->assertSame(8, Exercice2::run(2, 2, 2, 10));
    }
}