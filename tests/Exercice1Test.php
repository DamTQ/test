<?php

use PHPUnit\Framework\TestCase;

final class Exercice1Test extends TestCase
{
    public function testRun(): void
    {
        $this->assertSame('2', Exercice1::run(2));
        $this->assertSame('1', Exercice1::run(1));
        $this->assertSame('0', Exercice1::run(0));
        $this->assertSame('Crop', Exercice1::run(3));
        $this->assertSame('Crop', Exercice1::run(6));
        $this->assertSame('Co', Exercice1::run(5));
        $this->assertSame('Co', Exercice1::run(10));
        $this->assertSame('Crop & Co', Exercice1::run(15));
        $this->assertSame('Crop & Co', Exercice1::run(30));
        $this->assertSame('Crop & Co', Exercice1::run(60));
    }
}