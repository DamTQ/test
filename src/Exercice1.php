<?php

final class Exercice1
{
    public static function run(int $number): string
    {
        if (0 === $number) {
            $result = 0;
        } elseif (0 === $number % 15) {
            $result = "Crop & Co";
        } elseif (0 === $number % 3) {
            $result = "Crop";
        } elseif (0 === $number % 5) {
            $result = "Co";
        } else {
            $result = $number;
        }

        return $result;
    }
}
