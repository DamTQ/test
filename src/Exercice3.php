<?php

final class Exercice3
{
    /**
     * @param string[] $notes liste des 3 notes (séparées par des espaces) pour chaque restaurant
     * @return int|null la note la plus élevée arrondie à l'entier supérieur (null s'il n'y a aucune note en entrée)
     */
    public static function run(array $notes): ?int
    {
        // calculer la moy de chaque restaurant arrondis à l entier supérieur.
        $noteLaPlusEleve = 0;
        foreach ($notes as $note) {
            $moyenneNotesRestaurant = ceil(
                array_sum(
                    explode(' ', $note)
                )
                /3
            );
            $noteLaPlusEleve = max($noteLaPlusEleve, $moyenneNotesRestaurant);
        }
        return $noteLaPlusEleve;
    }
}
