<?php

final class Exercice2
{
    public static function run(int $taille1, int $taille2, int $taille3, int $taille4): int
    {
        $tailles = [$taille1, $taille2, $taille3, $taille4];

        return array_sum($tailles) - 4 * min($tailles);
    }
}