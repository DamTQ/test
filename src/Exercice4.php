<?php

final class Exercice4
{
    public static function run(string $cases)
    {
        // il faut que P soit supérieur au nombre de case du plus grand fossé.
        $fosses = preg_split('(-+)', $cases, 0, PREG_SPLIT_NO_EMPTY);
        if (empty($fosses)) {
            $result = 1;
        } else {
            $listeLargeurFosse = array_map(
                fn ($fosse) => substr_count($fosse, '_'),
                $fosses
            );
            $result = max($listeLargeurFosse) + 1;
        }

        return $result;
    }
}
